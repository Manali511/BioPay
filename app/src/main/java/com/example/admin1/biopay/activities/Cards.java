package com.example.admin1.biopay.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.example.admin1.biopay.R;
import com.example.admin1.biopay.database.DBHelper;

public class Cards extends AppCompatActivity implements View.OnClickListener , GestureDetector.OnDoubleTapListener{

    RelativeLayout relativeLayout;
    LinearLayout cardsHolder;
    String adharNo;
    DBHelper dbHelper;
    Cursor cursor;
    int id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cards);
        relativeLayout = (RelativeLayout) findViewById(R.id.cards_ll);
        cardsHolder = (LinearLayout) findViewById(R.id.cards_layout);
        Bundle bun = getIntent().getExtras();
        dbHelper = new DBHelper(this);
        adharNo = bun.getString("adharNum");
        try {
             cursor = dbHelper.getCardNumbers(adharNo);
             if (cursor != null && cursor.moveToFirst()) {
                Log.e("cards", "cursor true");
            } else {
                Log.e("cards", "cursor false");
            }

            RadioGroup radioGroup = new RadioGroup(this);

            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );


            int i = 0;
            do {
                Log.e("cards", "in while loop " + i);
                RadioButton radioButtonView = new RadioButton(this);
                radioButtonView.setText(cursor.getString(cursor.getColumnIndex("cardNumber")));
                radioButtonView.setOnClickListener(this);
                radioButtonView.setId(i);
                i++;
                radioGroup.addView(radioButtonView, p);

            }while (cursor.moveToNext());
            cardsHolder.addView(radioGroup, p);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        id = v.getId();
        Log.e("view id", " "+id);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter PIN");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int pine = Integer.parseInt(input.getText().toString());
                cursor.moveToPosition(id);
                int actual_pin = cursor.getInt(cursor.getColumnIndex("cardPin"));

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }
}
