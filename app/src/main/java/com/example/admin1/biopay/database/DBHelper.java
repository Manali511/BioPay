package com.example.admin1.biopay.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    static final String DATABASE_NAME = "BioPay.db";
    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists bioPayTable (id integer primary key autoincrement,adharCardNo text, cardNumber integer, cardHolderName text, cardPin integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists bioPayTable");
        onCreate(db);
    }

    public void insertData(String adharCardNo, int cardNo, String holderName, int cardPin){
        SQLiteDatabase db = this.getWritableDatabase();
        Log.e("insert", "a "+adharCardNo+" num "+cardNo+" holder :"+holderName+" pin: "+cardPin);
        db.execSQL("insert into bioPayTable(adharCardNo, cardNumber,cardHolderName, cardPin) values ('"+adharCardNo+"', '"+cardNo+"','"+holderName+"','"+cardPin+"')");
    }

    public Cursor getCardNumbers(String adharNo){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cor;
        Log.e("dbhelper", ""+adharNo);
        try{
            cor = db.rawQuery("select * from bioPayTable where adharCardNo = '" +adharNo+ "'", null);}
        catch(SQLiteException sq){
            sq.printStackTrace();
            cor = null;
        }
        return cor;
    }
}
