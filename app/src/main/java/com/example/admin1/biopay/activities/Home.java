package com.example.admin1.biopay.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.admin1.biopay.R;
import com.example.admin1.biopay.database.DBHelper;

public class Home extends AppCompatActivity {

    EditText adhar;
    Button sub;
    LinearLayout linearLayout;
    String adharNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        adhar = (EditText) findViewById(R.id.adharcardNo);
        sub = (Button) findViewById(R.id.submit);
        linearLayout = (LinearLayout) findViewById(R.id.home_ll);
        populateDatabase();
        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adharNum = adhar.getText().toString().trim();
                if (adharNum.equals("")) {
                    Snackbar.make(linearLayout, "Aadhar Number", Snackbar.LENGTH_SHORT).show();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("adharNum", adharNum);
                    Intent i = new Intent(Home.this, Cards.class);
                    i.putExtras(bundle);
                    startActivity(i);
                }
            }
        });
    }
    private void populateDatabase(){
        DBHelper db = new DBHelper(this);
        Log.e("home" ,"int populate");
        db.insertData("1234", 123, "Archana Agrawal", 1234);
        db.insertData("5678", 567, "Deepti", 5678);
        db.insertData("1357", 135, "Sayali Kane", 135);
        db.insertData("1357", 357, "Sayali Pawar", 357);
    }
}
